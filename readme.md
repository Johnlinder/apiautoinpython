**What you need to run the tests:**

You need a Virtual environment with python. So download python. I am using the latest version of python 3.9, but some older versions will probably work to.

You also need the dependencies specified in requirements.txt.
If your environment doesn't download them automatically for you, then you need to write
$ pip install {requirement}

To save all your current requirements in the file, write $ pip freeze > requirements.txt

**Now you should be set to run the tests from command line.**

Here are some examples of what you could write from the folder containing thetests.py:

$ pytest thetests.py (normal run)

$ pytest thetests.py -m command (runs all tests with the pytest command marker on them(which is all commands))

$ pytest thetests.py -m queries (the same for all queires)

$ pytest thetests.py --junitxml=C:\reports\junitxmlreport.xml (produces a junitxml report stored in specified folder)
