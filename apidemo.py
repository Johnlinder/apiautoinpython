import csv
import json
import pytest
from jsonschema import validate
import requests
from requests.auth import HTTPBasicAuth


def test_CovidCountries():

     requestdata = {
}
     payload = json.dumps(requestdata)
     headers = {
         'Content-Type': 'application/json'
     }
     try:
        response = requests.request("GET", 'https://api.covid19api.com/countries', auth=HTTPBasicAuth('Username', 'Password'), headers=headers, data=payload)
     except requests.exceptions.ConnectionError:
         print("Internet connection down")
     else:
         jsonresponse= response.json()

         print(jsonresponse)

     assert response.status_code == 200
     schema2 = {
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "items": {
        "$id": "#/items",
        "anyOf": [
            {
                "$id": "#/items/anyOf/0",
                "type": "object",
                "required": [ "Country",
                                "Slug",
                                 "ISO2"],
                "properties": {
                    "Country": {
                        "$id": "#/items/anyOf/0/properties/Country",
                        "type": "string",
                         "minLength": 3,
                         "maxLength": 70
                    },
                    "Slug": {
                        "$id": "#/items/anyOf/0/properties/Slug",
                        "type": "string",
                    },
                    "ISO2": {
                        "$id": "#/items/anyOf/0/properties/ISO2",
                        "type": "string",
                        "pattern": "^[A-Z]{2,2}$"
                    }
                }
            }
        ]
    }
}


     validate(jsonresponse, schema2)

with open('csvtestdata/test_CovidCountries.csv') as f:
 reader = csv.reader(f)
 addAgentData = list(reader)

@pytest.mark.parametrize("country, timespan", addAgentData)
def test_CovidCountries_byCountry(country, timespan):
    requestdata = {
    }
    payload = json.dumps(requestdata)
    headers = {
        'Content-Type': 'application/json'
    }
    try:
        response = requests.request("GET", 'https://api.covid19api.com/country/'+country+'/status/confirmed?from='+timespan+'',
                                    auth=HTTPBasicAuth('Username', 'Password'), headers=headers, data=payload)
    except requests.exceptions.ConnectionError:
        print("Internet connection down")
    else:
        jsonresponse = response.json()

        print(jsonresponse)

    assert response.status_code == 200
    schema2 = {}

    validate(jsonresponse, schema2)

with open('csvtestdata/test_Userregresin.csv') as f:
 reader = csv.reader(f)
 addAgentData= list(reader)
@pytest.mark.parametrize("name, job, movie", addAgentData)
def test_createUserRegresIn(name, job, movie):

     requestdata = {
    "name": name,
    "job": job,
    "movies": ["I Love You Man", movie]
}
     payload = json.dumps(requestdata)
     headers = {
         'Content-Type': 'application/json'
     }
     try:
        response = requests.request("POST", 'https://reqres.in/api/users', headers=headers, data=payload)
     except requests.exceptions.ConnectionError:
         print("Internet connection down")
     else:
         jsonresponse= response.json()

         print(jsonresponse)
     assert response.status_code == 201
     schema2 = {}

     validate(jsonresponse, schema2)